@echo off
set CP=lib\*;classes
set SP=src\java\

del nxt.jar /F /Q
del nxtservice.jar /F /Q
rmdir classes /S /Q
md classes
rmdir addons\classes /S /Q
md addons\classes

dir .\src\*.java /s /B > temp.list

javac -Xlint:unchecked -encoding utf8 -sourcepath %SP% -classpath %CP% -d classes @temp.list
::javac -encoding utf8 -sourcepath "%SP%" -classpath "%CP%" -d classes\ src\java\nxt\*.java src\java\nxt\*\*.java src\java\nxt\*\*\*.java src\java\nxtdesktop\*.java

echo "nxt class files compiled successfully"

::ls addons/src/*.java > /dev/null 2>&1 || exit 0
javac -encoding utf8 -sourcepath %SP% -classpath %CP% -d addons\classes addons\src\*.java

echo "addon class files compiled successfully"
del temp.list /F /Q