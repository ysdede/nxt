/******************************************************************************
 * Copyright © 2013-2016 The Nxt Core Developers.                             *
 * Copyright © 2016-2017 Jelurida IP B.V.                                     *
 *                                                                            *
 * See the LICENSE.txt file at the top-level directory of this distribution   *
 * for licensing information.                                                 *
 *                                                                            *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,*
 * no part of the Nxt software, including this file, may be copied, modified, *
 * propagated, or distributed except according to the terms contained in the  *
 * LICENSE.txt file.                                                          *
 *                                                                            *
 * Removal or modification of this copyright notice is prohibited.            *
 *                                                                            *
 ******************************************************************************/

RemoteNodesManager.prototype.REMOTE_NODES_BOOTSTRAP =
    {
        "peers": [
            {
                "hallmark": "e5e172cea845c79976a2498c47d4c77b5088f088a9f1ee630629520c51e4cb1613006e78746e6f64653030342e64646e73732e6575e80300006ec7330137125334a4545c772054b6bac4401ad8d3bef24b9b6d93e5d5e59729a56434cb0dbe48d01a180f105c92c4376c254a1d26c196f33a419870bc7b1bf497062d8f8d",
                "downloadedVolume": 7660652,
                "address": "195.181.243.224",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 2458,
                "uploadedVolume": 25141131,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.8",
                "platform": "NXT-QEP7-6PBP-Z8HH-5E8QB",
                "inboundWebSocket": true,
                "apiSSLPort": 7876,
                "lastUpdated": 125216322,
                "blacklisted": false,
                "announcedAddress": "nxtnode004.ddnss.eu",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124926337,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "e5e172cea845c79976a2498c47d4c77b5088f088a9f1ee630629520c51e4cb1613006e78746e6f64653030352e64646e73732e6575e80300006ec733014d1889de7dc22777ca5c46a2d2e229d6b7b80b0aa4e4c26ee9377963d702246c00843beeec68089d375b740bfc2ce4d72ddb3e8a39fb2b790c9737f50aa6adb0d5",
                "downloadedVolume": 6055101,
                "address": "94.176.235.119",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 2458,
                "uploadedVolume": 5772903,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.8",
                "platform": "NXT-QEP7-6PBP-Z8HH-5E8QB",
                "inboundWebSocket": false,
                "apiSSLPort": 7876,
                "lastUpdated": 125213524,
                "blacklisted": false,
                "announcedAddress": "nxtnode005.ddnss.eu",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 122589877,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "a36a3b65e5626527e0d4e547c4f020c5382b7c36c646f43a7c1432f0138cfb580d0033362e35352e3234322e31333710270000dfc73301f4f7cf76f0b7342b0b4cfc8569e6b1959aac47f1d4e48da53282e433c43ae9360eb71cab0f35d15af57f40f80f2cb06533f68816cafb3ec6a6105b82a31d8ad8c0",
                "downloadedVolume": 833456,
                "address": "36.55.242.137",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 13466,
                "uploadedVolume": 2675676,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-2F5C-J8HD-3R48-286TA",
                "inboundWebSocket": true,
                "apiSSLPort": 7875,
                "lastUpdated": 125217297,
                "blacklisted": false,
                "announcedAddress": "36.55.242.137",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124937688,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "79118ecab75786f3a47092f8b3f720939bd6df7b83c15b67eddc39667221dc6f0f003136372e3136302e3138302e31393901000000b6c63301ebbcd8d6db9cbac687791d8c34a20185bd9d641b48b99ec027a6780545e92a41015864c604ad7eb2daa36a06353cc47396bc7e6fb01b07f30f49ce1271f5af57a6",
                "downloadedVolume": 1373449,
                "address": "167.160.180.199",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 811623,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-Y8BY-8RSY-KEMX-BTAZC",
                "inboundWebSocket": true,
                "apiSSLPort": 7878,
                "lastUpdated": 125215909,
                "blacklisted": false,
                "announcedAddress": "167.160.180.199",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 125109889,
                "state": 0,
                "shareAddress": true
            },
            {
                "downloadedVolume": 6674200,
                "address": "192.3.196.10",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 88562510,
                "services": [
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT",
                "inboundWebSocket": true,
                "apiSSLPort": 9110,
                "lastUpdated": 125218225,
                "blacklisted": false,
                "announcedAddress": "192.3.196.10",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": true,
                "lastConnectAttempt": 125218225,
                "state": 1,
                "shareAddress": true
            },
            {
                "hallmark": "f3e7e61f6fd9f59f7297f2baa679114490c0d7b8112f3a0cd62123072f3541360c0034372e38392e31392e31393910270000ebc7330149a8f6a353f5b0a314bef1dd7c445f10e0ff23465d404c5cde70e30e672bc82a0c90de66ea772606a0d7a005b73e5b11f51c3f6db9819dc36500905d4d01713c96",
                "downloadedVolume": 29629977,
                "address": "47.89.19.199",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 1804,
                "uploadedVolume": 48936333,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-WTMQ-VMKW-WS4Q-A7S8L",
                "inboundWebSocket": true,
                "apiSSLPort": 7876,
                "lastUpdated": 125217410,
                "blacklisted": false,
                "announcedAddress": "47.89.19.199",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124460421,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "77c01f7ef134678bbbd8b1f4c64ae88df2a406e7d2d91fb5331602a6ce56c6220b003137362e392e38342e3530e80300004bc8330162be73f86e0a6e6f78da0e2a614ecfd125de1a52d8d0a286e4a448fb99ea31f003b71b7a0170f807aed56fdfb3daf77c4a869810d53a8d8ccedbcc8263569b3e15",
                "downloadedVolume": 1650744,
                "address": "176.9.84.50",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 1451,
                "uploadedVolume": 1404049,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-RS55-6KN3-PUUZ-3TT5H",
                "inboundWebSocket": true,
                "apiSSLPort": 7876,
                "lastUpdated": 125216639,
                "blacklisted": false,
                "announcedAddress": "176.9.84.50",
                "apiPort": 7875,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 123644946,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "ce271d8c2c38e44058c4205f1efa433319fe5645fe9ff78a51dfbd366e7064500e003136372e3136302e3138302e353801000000bdc63301e5d6ba8aa875c07f91324d3a4cc806f818a0880bbdb3fa8f20bce342829c497006edce97a611826452b44bd4e96d3b37cc9d7e1d0d69bab55e81a1c01424fd9ce1",
                "downloadedVolume": 1767344,
                "address": "167.160.180.58",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 3,
                "uploadedVolume": 991073,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-TGFQ-U33C-C37U-CMKWF",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125216464,
                "blacklisted": false,
                "announcedAddress": "167.160.180.58",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124340734,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "ce271d8c2c38e44058c4205f1efa433319fe5645fe9ff78a51dfbd366e7064500f003136372e3136302e3138302e32303401000000bdc63301481adf269042741bb2c242436d3d30a15101a6bb1adadd9fbec82fa37e78008a087648b3f2392fc69dc3dc9b711f8861348f3b8d268a3b6ee9cabc70e989633f92",
                "downloadedVolume": 1619796,
                "address": "167.160.180.204",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 3,
                "uploadedVolume": 825418,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-TGFQ-U33C-C37U-CMKWF",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125216012,
                "blacklisted": false,
                "announcedAddress": "167.160.180.204",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124195866,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "ce271d8c2c38e44058c4205f1efa433319fe5645fe9ff78a51dfbd366e7064500f003136372e3136302e3138302e32303601000000bdc6330147c3f43058f826434773504cb68f598d438ae48b3e45e3240eaaecbaf8623e970734f2e58ba511cac39554d6bb29fa77ad23641075b175f1606a5f7c57ee5b4d96",
                "downloadedVolume": 1581781,
                "address": "167.160.180.206",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 3,
                "uploadedVolume": 766642,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-TGFQ-U33C-C37U-CMKWF",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125214396,
                "blacklisted": false,
                "announcedAddress": "167.160.180.206",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 119713958,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "a470c76d15e19315905e5dc6c1f2e9a90fbc29d8b0d98d571fc641e750f6147b0c003133302e3231312e37362e3532000000a2c63301e3e199be96e50fbc9af51a934e6e1827dff5d8f6b0d4019c68404f07cccc50670ecf811ec2dbf091a56bd0285d4e218789b801ba892b63ffdf55dd3c342f0c2d65",
                "downloadedVolume": 24019854,
                "address": "130.211.76.5",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 10631,
                "uploadedVolume": 4775969,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-KDPE-5EZ8-VCU8-65ADP",
                "inboundWebSocket": true,
                "apiSSLPort": 7878,
                "lastUpdated": 125216690,
                "blacklisted": false,
                "announcedAddress": "130.211.76.5",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124339434,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "51a38c9129aca7b80bd1636aa137a5a7014029084760f324d4139b423e2fd31b0b006e78742e6e6f69702e6d65640000008a2d3301766f20510f3bd8cbfd3374d3bc0ea234b5d818dffd8eecc103601eae55f7e579098ad71d8eab899706c2797f93b1b8dad56db586ebb98c87a04a659918823c23b1",
                "downloadedVolume": 3463209,
                "address": "178.150.207.53",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 444,
                "uploadedVolume": 3189102,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "Windows 7 amd64",
                "inboundWebSocket": true,
                "apiSSLPort": 7877,
                "lastUpdated": 125218105,
                "blacklisted": false,
                "announcedAddress": "nxt.noip.me",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 125109889,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "9b34ff42194bb0bc5304a9833086fff62dc617dc804e33e1bf7aeacf82529e600f003139332e3135312e3130362e313239801a060026a13301fd13413ca86d595d723b192892eed752c627db293bd5a46a6a6c5d5a3397c1350d26cf3d7dcd506c0d892580bb8c4aae703fe46968e4048f8c82c9d49e21160ab3",
                "downloadedVolume": 8537112,
                "address": "193.151.106.129",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 408,
                "uploadedVolume": 2623267,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-WCMK-8HAF-FZ7U-3U5FZ",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125179737,
                "blacklisted": false,
                "announcedAddress": "193.151.106.129",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 125109929,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "ab7b87c64a56c791cd958d3c00a7adaff0a0e502f7c7f50741196e53f44bdc3d13006e78746e6f64652e67656973737765622e6465102700006ec73301cf7bb80a98ac2398711d0ff5c9c766e12cc1afdf1c7bfdeeba12339cc6175e0603cb3e214bbfb25aa3fa690c3c2569e4429d57801babdb40aa03268d4ceff74a61",
                "downloadedVolume": 2839461,
                "address": "178.201.21.19",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 6612,
                "uploadedVolume": 1369570,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-C8LS-F9H9-TU9S-7VTXE",
                "inboundWebSocket": false,
                "apiSSLPort": 7876,
                "lastUpdated": 125217753,
                "blacklisted": false,
                "announcedAddress": "nxtnode.geissweb.de",
                "apiPort": 7875,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 123471380,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "ce271d8c2c38e44058c4205f1efa433319fe5645fe9ff78a51dfbd366e7064500d003130342e3232332e35332e313401000000bdc6330166dd9e40baf634ecb99669f80f79507c04e46567406f02f22e50118f9074e7f80fad49b8bad14d5a63dc4bb75e782f7d245d343a57fdcf916d8a380f70015a6d7a",
                "downloadedVolume": 4418659,
                "address": "104.223.53.14",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 3,
                "uploadedVolume": 3025625,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-TGFQ-U33C-C37U-CMKWF",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125217425,
                "blacklisted": false,
                "announcedAddress": "104.223.53.14",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 122973020,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "e5e172cea845c79976a2498c47d4c77b5088f088a9f1ee630629520c51e4cb1613006e78746e6f64653030332e64646e73732e6575e80300006ec7330109b03be4cbab5400c42f9143a1a26f2449a285f061ea7b33acb1d935c6515ee00582688c208f5444e3a285640d208b301a74ce07bab7d713f7b4af486282a1a5e2",
                "downloadedVolume": 4365371,
                "address": "212.24.98.206",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 2458,
                "uploadedVolume": 1614495,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.8",
                "platform": "NXT-QEP7-6PBP-Z8HH-5E8QB",
                "inboundWebSocket": false,
                "apiSSLPort": 7876,
                "lastUpdated": 125216500,
                "blacklisted": false,
                "announcedAddress": "nxtnode003.ddnss.eu",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124632677,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "0af12064cd069c0c8f7540a0088b6708dbe79a055e389a9c0926c896d1a5f42d0d0038392e3231372e34392e313935a086010047c8330115af762c685bf2891f2286f3ba051622f72c18026627ecbc0e82a1ec7bccff54026026ae211f51f8d9fbdb6492eeecc835de2750c7209e3a0f9a52b648111a645c",
                "downloadedVolume": 23006700,
                "address": "89.217.49.195",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 12828,
                "uploadedVolume": 7465165,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-6F8N-ZQB5-KZAQ-32MAP",
                "inboundWebSocket": false,
                "apiSSLPort": 7878,
                "lastUpdated": 125218353,
                "blacklisted": false,
                "announcedAddress": "89.217.49.195",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 125048317,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "060968adb5486ffb2ee9a86ac4f924944cd8c42affded0e3983912a37ca993070e003136332e3137322e3135342e373401000000c7a233015fae057a64433cf101eacccd79a006674be885f848b05b49f905b42ff3198ce7033f54b417ea6c20a580c0c14b0f576ce0bd846f475fac5e01b1a67a9aea4f895b",
                "downloadedVolume": 100341794,
                "address": "163.172.154.74",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 40741402,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-LJ8T-XBBV-KDHS-66X3T",
                "inboundWebSocket": false,
                "apiSSLPort": 7876,
                "lastUpdated": 125217799,
                "blacklisted": false,
                "announcedAddress": "163.172.154.74",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124168904,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "34eac00f4ed254526676f4e1ae714c88d9d2810bef7b7d04018be5dedb6f55030e003138352e3137302e3131332e3739e8030000cec73301b6ec72bf7d7629acac42f37b5d3dd09f2137b3f49bad69510750de8558978a7b0fe6518548fba232f2b0edef304c0502eafabe1ff1c555a069fdc2ef599509b5d6",
                "downloadedVolume": 2550072,
                "address": "185.170.113.79",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 1716,
                "uploadedVolume": 1065376,
                "services": [
                    "HALLMARK",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-X8RW-KAH7-DKK2-6VSR7",
                "inboundWebSocket": true,
                "apiSSLPort": 7877,
                "lastUpdated": 125215985,
                "blacklisted": false,
                "announcedAddress": "185.170.113.79",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": true,
                "lastConnectAttempt": 125215985,
                "state": 1,
                "shareAddress": true
            },
            {
                "hallmark": "c6ec505cdd554b6f9008e29e29a3e7ceeb45df3d0814c0e58e6b7b7efddefb5c0d006e78742e726573756c742e646564000000caa03301ba5599e9295529df34200a2dc2916ba61ca9b474c288e22c7586e8ff77a1e21405711f0530e3d41ee3077375711d600dc6a819b090b792da59477bc528423c23dc",
                "downloadedVolume": 22681135,
                "address": "78.94.2.74",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 16738,
                "uploadedVolume": 12397185,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-7NNQ-9EKA-YDGC-8C3W6",
                "inboundWebSocket": true,
                "apiSSLPort": 7878,
                "lastUpdated": 125218296,
                "blacklisted": false,
                "announcedAddress": "nxt.result.de",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124937729,
                "state": 0,
                "shareAddress": true
            },
            {
                "downloadedVolume": 2278683,
                "address": "51.15.71.180",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 1345805,
                "services": [
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "Linux amd64",
                "inboundWebSocket": false,
                "apiSSLPort": 7876,
                "lastUpdated": 125151776,
                "blacklisted": false,
                "announcedAddress": "51.15.71.180",
                "apiPort": 7877,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124373917,
                "state": 2,
                "shareAddress": true
            },
            {
                "hallmark": "6984e56f5b13e408311481b82c87257fa360e36155fe73c38d34f53b1403131e0d0032332e39342e3133342e3136315a000000bda033017ceda8b30aae929190704ff817038abf08a32effa8374298437a3e704c1a47cc0c54ab6b58cb2c86bd06f9651a70314cf444be279b67e759248cfa9ae8d64bd66f",
                "downloadedVolume": 279379,
                "address": "23.94.134.161",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 1,
                "uploadedVolume": 187120,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "Ardor",
                "inboundWebSocket": false,
                "apiSSLPort": 7877,
                "lastUpdated": 125216890,
                "blacklisted": false,
                "announcedAddress": "23.94.134.161",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 0,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "a36a3b65e5626527e0d4e547c4f020c5382b7c36c646f43a7c1432f0138cfb581000676f6f666f66662e6d79646e732e627a102700008dc53301729242b9d6f0809b92b1133930ff4ef5f760effa008252626f01f6e24d0fba0a030623a435bcbd7920fd6eab4c1d8d6a1ea847ab8908b0ff0b1e069576075780f9",
                "downloadedVolume": 6075652,
                "address": "211.7.125.35",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 2357701,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-2F5C-J8HD-3R48-286TA",
                "inboundWebSocket": false,
                "apiSSLPort": 7875,
                "lastUpdated": 125215085,
                "blacklisted": false,
                "announcedAddress": "goofoff.mydns.bz",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": true,
                "lastConnectAttempt": 125215085,
                "state": 1,
                "shareAddress": true
            },
            {
                "hallmark": "e79f427dc78bf604418b77848116d8327aa5b9d01d6ce17fabbc53a98a06275e0e0039342e3137372e3139362e31333417000000d0c733011d82a23b9f1482ef84aaece87bf632dd51ce4d293c94b6f31348b8e43575b4540fbbe3567e298a261ea72381691f8ddda07914dd437c4a064c5a7d45d7e187bbdb",
                "downloadedVolume": 1627642,
                "address": "94.177.196.134",
                "inbound": false,
                "blockchainState": "UP_TO_DATE",
                "weight": 2,
                "uploadedVolume": 2576503,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-MQ6Q-LF3U-FWRT-59EFL",
                "inboundWebSocket": false,
                "apiSSLPort": 7877,
                "lastUpdated": 125218245,
                "blacklisted": false,
                "announcedAddress": "94.177.196.134",
                "apiPort": 7876,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": true,
                "lastConnectAttempt": 125218245,
                "state": 1,
                "shareAddress": true
            },
            {
                "hallmark": "949f52e60fc170698ba496aa4ce4914006878555048bd60808e459621de740201100676f6c64737461722e64646e732e6e6574640000004cc83301ab7ac206e915b5c36d537f78cd8825e8886c7014ac5128b97d9d2fd3aa904b81039e6a6b777cf178f0c2835c63181a40418edee28a7130e34b87fb11c80669c032",
                "downloadedVolume": 3985693,
                "address": "91.145.96.142",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 0,
                "uploadedVolume": 39719986,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.9",
                "platform": "NXT-JZZD-P5S2-GPCK-HMSVB",
                "inboundWebSocket": true,
                "apiSSLPort": 7876,
                "lastUpdated": 125217255,
                "blacklisted": false,
                "announcedAddress": "goldstar.ddns.net",
                "apiPort": 7878,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": false,
                "lastConnectAttempt": 124815774,
                "state": 0,
                "shareAddress": true
            },
            {
                "hallmark": "9de572354d14040c4936a79329ad1ae0ad374545ab8e77d5c14edb05f59138460d006e7874322e757475742e6f7267580200002da13301199401641f68551242c4232078245d87dc8dd328fe70e867eb6dc25a31481f3c04e35cd07a41716f945603eca9a4295bf18e6c1ac28b2bf9e984dcd51b66382bed",
                "downloadedVolume": 21258211,
                "address": "176.9.24.36",
                "inbound": true,
                "blockchainState": "UP_TO_DATE",
                "weight": 4095,
                "uploadedVolume": 9808032,
                "services": [
                    "HALLMARK",
                    "PRUNABLE",
                    "API",
                    "API_SSL",
                    "CORS"
                ],
                "version": "1.11.8",
                "platform": "NXT-DBSU-KJ8N-BE67-FXX98",
                "inboundWebSocket": true,
                "apiSSLPort": 7876,
                "lastUpdated": 125215865,
                "blacklisted": false,
                "announcedAddress": "nxt2.utut.org",
                "apiPort": 7877,
                "application": "NRS",
                "port": 7874,
                "outboundWebSocket": true,
                "lastConnectAttempt": 125215865,
                "state": 1,
                "shareAddress": true
            }
        ],
        "requestProcessingTime": 23
    };